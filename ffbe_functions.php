<?php
	function cleantext($text){
		$text=trim($text);

		$text=str_replace("'", "", $text);
		$text=str_replace("-", "", $text);
		$text=str_replace("_", "", $text);
		$text=str_replace(",", "", $text);
		$text=str_replace(" ", "", $text);
		$text=str_replace("/", "", $text);
		$text=str_replace("\\", "", $text);
		$text=str_replace("*", "", $text);
		$text=str_replace("+", "", $text);

		$text=str_replace("é", "e", $text);
		$text=str_replace("è", "e", $text);
		$text=str_replace("ë", "", $text);

		$text=str_replace("1", "", $text);
		$text=str_replace("2", "", $text);
		$text=str_replace("3", "", $text);
		$text=str_replace("4", "", $text);
		$text=str_replace("5", "", $text);
		$text=str_replace("6", "", $text);
		$text=str_replace("7", "", $text);
		$text=str_replace("8", "", $text);
		$text=str_replace("9", "", $text);
		$text=str_replace("0", "", $text);

		return $text;
	}

	function generateusername($name, $firstname, $users){
		$name=strtolower($name);
		$firstname=strtolower($firstname);

		$name=cleantext($name);
		$firstname=cleantext($firstname);

		$username=substr($name,0,3).$firstname;

		$postfix=mt_rand(317,981);
		
		$username=$username.$postfix;

		if(in_array($username,$users)){
			$username=generateusername($name,$firstname,$users);
		}

		return $username;	
	}

	function randomPassword(int $passlength=NULL) {
		if($passlength==NULL)$passlength=6;
	    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	    $pass = array(); //remember to declare $pass as an array
	    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
	    for ($i = 0; $i <= $passlength; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $alphabet[$n];
	    }
	    return (string)implode($pass); //turn the array into a string
	}
?>