<?php
	ob_start();
	define("IN_PHPBB", true);
	session_start();

	#Database
	echo "> Set variables...<br/>";

	define("HOSTNAME","127.0.0.1");
	define("USERNAME","root");
	define("PASSWORD","");
	define("DATABASE","nut_fitness");
	define("TABLENAME","users");
	define("TABLEPREFIX","v2aJZn_");
	define("FFTABLE",TABLEPREFIX.TABLENAME);

	require("ffbe_functions.php");

	#phpBB
	echo "> Set phpBB globals...<br/>";
	global $config, $db, $user, $auth, $template, $phpbb_root_path, $phpEx, $cache;
	$phpbb_root_path = "./";
	$phpEx = "php";
	
	echo "> Include " . $phpbb_root_path . "...<br/>";
	include($phpbb_root_path . "includes/functions_user.php");
	//include($phpbb_root_path . "includes/functions.php");
	include($phpbb_root_path . "common.php");
	echo "Included!<br/>";

	echo "> Start phpBB methods...<br/>";
	$user->session_begin();
	$auth->acl($user->data);
	$user->setup();

	#Export File
	echo "> Define CSV-file...<br/>";
	define("FILENAME","LaunchRock_Export_20140803_184016_UTC.csv");
	define("FILE_DELIM",",");

	#Read Users
	if(!file_exists(FILENAME)){ echo "<p><strong>" . FILENAME . "</strong> does not exist!</p>"; exit; }
	$handle=fopen(FILENAME,"r");
	if(!$handle){ echo "<p>Could not open file!</p>"; exit; }
	$size=filesize(FILENAME);
	if(!$size){ echo "<p>File is empty.</p>"; exit;}

	$csvcontent = array_map('str_getcsv', file(FILENAME));

	fclose($handle);

	#MySQL Connection
	echo "> Connecting...";
	$ffbe_con=mysql_connect(HOSTNAME,USERNAME,PASSWORD) or die("<br/>Could not make a connection: " . mysql_error() . "<br/>");
	echo "Connected!<br/>";

 	$lines = 0; $doubles=0; $insertedusers = array();

 	echo "> Save new users...";
 	foreach($csvcontent as $userdata){

		if($lines>0){
			$ffbe_regdate=new DateTime($userdata[0]);
			$ffbe_newuser=array(
				"username" => generateusername($userdata[6],$userdata[5],$insertedusers),
				"password" => randomPassword(),
				"email" => $userdata[1],
				"regdate" => $ffbe_regdate->getTimestamp(),
				"name" => $userdata[6],
				"firstname" => $userdata[5],
				);

			#Register new User
			$username = $ffbe_newuser[username];
			$password = $ffbe_newuser[password];
			$email = $ffbe_newuser[email];
			$ffbe_regdate = $ffbe_newuser[regdate];
			$user_inactive_reason = 0;
			$user_inactive_time = 0;
			$user_type = USER_NORMAL;
			$user_row = array(
				"username" => $username,
				"user_password" => phpbb_hash($password),
				"user_email" => $email,
				"group_id" => 2, #Registered users group
				"user_timezone" => 1.00,
				"user_dst" => 1,
				"user_lang" => "nl",
				"user_type" => $user_type,
				"user_actkey" => "",
				//"user_dateformat" => "D M d, Y g:i a",
				"user_dateformat" => "d M Y, H:i",
				"user_style" => 5,
				"user_regdate" => $ffbe_regdate,
				"user_inactive_reason" => $user_inactive_reason,
				"user_inactive_time" => $user_inactive_time,
			);
			
			try{
				$phpbb_user_id = user_add($user_row);
				echo "U:".$username.", ";
				array_push($insertedusers,$ffbe_newuser);	
			}catch(exception $e){
				echo "D";
				$doubles++;
			}
		}

		$lines++;
		
	}
	
	#Create new CSV for MailChimp
	$filename = "./ffbe-mailchimp-users-list.csv";
	$handle = fopen($filename,"w");

	foreach($insertedusers as $user){
		fputcsv($handle, $user);
	}

	fclose($handle);

	#End
	echo "<br/>Done!<br/>";
	echo "<p><ul>
		<li>CSV Lines processed: " . $lines . "</li>
		<li>Users created: " . count($insertedusers) . "</li>
	</ul></p>";
	exit;
?>